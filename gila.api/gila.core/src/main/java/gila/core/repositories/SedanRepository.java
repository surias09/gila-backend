package gila.core.repositories;

import gila.core.entities.Sedan;
import org.springframework.stereotype.Repository;

@Repository
public interface SedanRepository extends VehicleRepository<Sedan> {
}
