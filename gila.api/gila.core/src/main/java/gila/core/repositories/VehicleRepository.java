package gila.core.repositories;


import gila.core.dtos.VehicleDto;
import gila.core.entities.Vehicle;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;

import java.util.UUID;

@NoRepositoryBean
public interface VehicleRepository<T extends Vehicle> extends CrudRepository<T, UUID> {

    @Query("SELECT new gila.core.dtos.VehicleDto(v.id, v.model, v.year, v.color, v.wheel) " +
            "FROM #{#entityName} v " +
            "WHERE v.model LIKE CONCAT('%', :model, '%') " +
            "AND v.active = true")
    Page<VehicleDto> findByModel(@Param("model") String model, Pageable pageable);

    @Modifying
    @Query("UPDATE #{#entityName} v SET v.active = false WHERE v.id = :id")
    void inactivate(@Param("id") UUID id);
}
