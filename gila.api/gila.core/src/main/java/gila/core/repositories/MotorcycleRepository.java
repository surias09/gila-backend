package gila.core.repositories;

import gila.core.entities.Motorcycle;
import org.springframework.stereotype.Repository;

@Repository
public interface MotorcycleRepository extends VehicleRepository<Motorcycle> {
}
