package gila.core.exceptions;

public class GenericException extends RuntimeException {

    public String code;

    public GenericException() { super(); }

    public GenericException(String code, String message) {
        super(message);
        this.code = code;
    }
}
