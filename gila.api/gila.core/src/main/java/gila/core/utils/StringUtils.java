package gila.core.utils;

public class StringUtils {

    public static Boolean isNullOrEmpty(String value) {
        return value == null || value.isEmpty();
    }
}
