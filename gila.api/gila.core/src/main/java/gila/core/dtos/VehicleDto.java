package gila.core.dtos;

import java.util.UUID;

public class VehicleDto {

    private UUID id;

    private String model;

    private int year;

    private String color;

    private int wheel;

    public VehicleDto(UUID id, String model, int year, String color, int wheel) {
        this.id = id;
        this.model = model;
        this.year = year;
        this.color = color;
        this.wheel = wheel;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getWheel() {
        return wheel;
    }

    public void setWheel(int wheel) {
        this.wheel = wheel;
    }
}
