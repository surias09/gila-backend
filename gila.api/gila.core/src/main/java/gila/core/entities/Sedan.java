package gila.core.entities;

import gila.core.enums.SedanCylinder;

import javax.persistence.*;

@Entity
@Table(name = "tbl_sedan")
public class Sedan extends Vehicle {

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private SedanCylinder motor;

    public Sedan() {
        this.setWheel(4);
    }

    public SedanCylinder getMotor() {
        return motor;
    }

    public void setMotor(SedanCylinder motor) {
        this.motor = motor;
    }
}
