package gila.core.entities;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "tbl_user")
public class AppUser {

    @Id
    @GeneratedValue
    @Column(length = 16)
    private UUID id;

    @Column(nullable = false, length = 50)
    private String username;

    @Column(nullable = false)
    private String password;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
