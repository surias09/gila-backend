package gila.core.entities;

import gila.core.enums.MotoCylinder;

import javax.persistence.*;

@Entity
@Table(name = "tbl_motorcycle")
public class Motorcycle extends Vehicle {

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private MotoCylinder motor;

    public Motorcycle() {
        this.setWheel(2);
    }

    public MotoCylinder getMotor() {
        return motor;
    }

    public void setMotor(MotoCylinder motor) {
        this.motor = motor;
    }
}
