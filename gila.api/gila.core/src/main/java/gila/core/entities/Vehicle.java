package gila.core.entities;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@MappedSuperclass
public abstract class Vehicle {

    @Id
    @GeneratedValue
    @Column(length = 16)
    private UUID id;

    @Column(nullable = false, length = 100)
    private String model;

    @Column(nullable = false)
    private int year;

    @Column(nullable = false, length = 20)
    private String color;

    @Column(nullable = false)
    private int wheel;

    @Column(nullable = false)
    private boolean active;

    @Column(nullable = false, updatable = false)
    private Date date;

    @PrePersist
    protected void beforeSave() {
        setDate(new Date());
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getWheel() {
        return wheel;
    }

    protected void setWheel(int wheel) {
        this.wheel = wheel;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Date getDate() {
        return date;
    }

    private void setDate(Date date) {
        this.date = date;
    }
}
