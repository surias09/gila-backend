package gila.core.services.impl;

import gila.core.services.SpringEncoderService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class SpringEncoderServiceImpl extends BCryptPasswordEncoder implements SpringEncoderService {

}
