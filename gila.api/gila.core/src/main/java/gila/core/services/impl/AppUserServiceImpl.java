package gila.core.services.impl;

import gila.core.entities.AppUser;
import gila.core.exceptions.GenericException;
import gila.core.repositories.AppUserRepository;
import gila.core.services.AppUserService;
import gila.core.services.SpringEncoderService;
import gila.core.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class AppUserServiceImpl implements AppUserService {

    @Autowired
    AppUserRepository repository;

    @Autowired
    SpringEncoderService springEncoderService;

    @Override
    public UUID save(AppUser entity) {

        if (StringUtils.isNullOrEmpty(entity.getUsername()) || StringUtils.isNullOrEmpty(entity.getPassword())) {
            throw new GenericException("BSN_REQUIRED_FIELDS", "Username and password are required");
        }

        entity.setPassword(springEncoderService.encode(entity.getPassword()));
        return repository.save(entity).getId();
    }
}
