package gila.core.services;

import gila.core.entities.Sedan;

public interface SedanService extends VehicleService<Sedan> {
}
