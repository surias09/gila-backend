package gila.core.services;

import gila.core.dtos.VehicleDto;
import gila.core.entities.Vehicle;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

public interface VehicleService<T extends Vehicle> {

    /**
     * Get Vehicles by model
     * @param model Model
     * @param pageable Pageable data
     * @return Page of vehicles
     */
    Page<VehicleDto> findByModel(String model, Pageable pageable);

    /**
     * Find by Id
     * @param id Vehicle identity
     * @return Vehicle data
     */
    T findById(UUID id);

    /**
     * Inactivate vehicle
     * @param id Identity
     */
    void inactivate(UUID id);

    /**
     * Save or update vehicle
     * @param entity Vehicle data
     * @return Identity
     */
    UUID save(T entity);
}
