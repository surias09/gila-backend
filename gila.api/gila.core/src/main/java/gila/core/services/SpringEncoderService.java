package gila.core.services;

public interface SpringEncoderService {

    /**
     * Encode password
     * @param rawPassword Characters to encode
     * @return Password encoded
     */
    String encode(CharSequence rawPassword);
}
