package gila.core.services.impl;

import gila.core.entities.Motorcycle;
import gila.core.repositories.MotorcycleRepository;
import gila.core.services.MotorcycleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MotorcycleServiceImpl extends VehicleServiceImpl<Motorcycle> implements MotorcycleService {

    @Autowired
    public MotorcycleServiceImpl(MotorcycleRepository repository) {
        super(repository);
    }
}
