package gila.core.services.impl;

import gila.core.dtos.VehicleDto;
import gila.core.entities.Vehicle;
import gila.core.exceptions.GenericException;
import gila.core.repositories.VehicleRepository;
import gila.core.services.VehicleService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

public abstract class VehicleServiceImpl<T extends Vehicle> implements VehicleService<T> {

    private final VehicleRepository<T> repository;

    protected VehicleServiceImpl(VehicleRepository<T> repository) {
        this.repository = repository;
    }

    @Override
    public Page<VehicleDto> findByModel(String model, Pageable pageable) {
        return repository.findByModel(model, pageable);
    }

    @Override
    public T findById(UUID id) {
        Optional<T> optional = repository.findById(id);

        if (!optional.isPresent()) {
            throw new GenericException("BSN_VEHICLE_NOT_FOUND", String.format("Vehicle: %s not found", id));
        }

        return optional.get();
    }

    @Transactional
    public void inactivate(UUID id) {
        repository.inactivate(id);
    }

    @Override
    public UUID save(T entity) {
        return repository.save(entity).getId();
    }
}
