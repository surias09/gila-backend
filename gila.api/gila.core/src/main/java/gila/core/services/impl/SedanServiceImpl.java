package gila.core.services.impl;

import gila.core.entities.Sedan;
import gila.core.repositories.SedanRepository;
import gila.core.services.SedanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SedanServiceImpl extends VehicleServiceImpl<Sedan> implements SedanService {

    @Autowired
    public SedanServiceImpl(SedanRepository repository) {
        super(repository);
    }
}
