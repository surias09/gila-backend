package gila.core.services;

import gila.core.entities.AppUser;

import java.util.UUID;

public interface AppUserService {

    /**
     * Create new user
     * @param entity User data
     * @return Identity
     */
    UUID save(AppUser entity);
}
