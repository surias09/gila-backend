package gila.core.services;

import gila.core.entities.Motorcycle;

public interface MotorcycleService extends VehicleService<Motorcycle> {
}
