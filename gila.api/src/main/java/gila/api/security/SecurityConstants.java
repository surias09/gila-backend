package gila.api.security;

public class SecurityConstants {
    public static final String SECRET = "GilaExercise";
    public static final long EXPIRATION_TIME = 900_000; // 5 minutes
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String HOME_URL = "/";
}
