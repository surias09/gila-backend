package gila.api.web.utils;

import gila.core.dtos.GenericExceptionDto;
import gila.core.exceptions.GenericException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {GenericException.class})
    protected ResponseEntity<GenericExceptionDto> handleConflict(RuntimeException ex, WebRequest request) {
        return new ResponseEntity(new GenericExceptionDto(((GenericException) ex).code, ex.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {Exception.class})
    protected ResponseEntity<GenericExceptionDto> handleConflict(Exception ex, WebRequest request) {
        return new ResponseEntity(new GenericExceptionDto("500", ex.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
