package gila.api.controllers;

import gila.core.dtos.VehicleDto;
import gila.core.entities.Vehicle;
import gila.core.services.VehicleService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

public abstract class VehicleController<T extends Vehicle> {

    private final VehicleService<T> service;

    public VehicleController(VehicleService<T> service) {
        this.service = service;
    }

    @GetMapping("pagination")
    public ResponseEntity<Page<VehicleDto>> getPagination(String model, Pageable pageable) {
        return ResponseEntity.ok(service.findByModel(model, pageable));
    }

    @GetMapping("{id}")
    public ResponseEntity<T> getById(@PathVariable() UUID id) {
        return ResponseEntity.ok(service.findById(id));
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Boolean> delete(@PathVariable() UUID id) {
        service.inactivate(id);
        return ResponseEntity.ok(true);
    }

    @PostMapping()
    public ResponseEntity<UUID> save(@RequestBody T entity) {
        return ResponseEntity.ok(service.save(entity));
    }

    @PutMapping()
    public ResponseEntity<UUID> update(@RequestBody T entity) {
        return ResponseEntity.ok(service.save(entity));
    }
}
