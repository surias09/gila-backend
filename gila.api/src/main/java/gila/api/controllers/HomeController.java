package gila.api.controllers;

import gila.core.entities.AppUser;
import gila.core.services.AppUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
public class HomeController {

    @Autowired
    private AppUserService service;

    @GetMapping()
    public String home() {
        return "Gila is running at version 0.0.1!";
    }

    @PostMapping()
    public ResponseEntity<Boolean> createUser(@RequestBody AppUser entity) {
        service.save(entity);
        return ResponseEntity.ok(true);
    }
}
