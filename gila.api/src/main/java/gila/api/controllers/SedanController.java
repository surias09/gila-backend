package gila.api.controllers;

import gila.core.entities.Sedan;
import gila.core.services.SedanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController()
@RequestMapping("api/sedan")
public class SedanController extends VehicleController<Sedan> {

    @Autowired
    public SedanController(SedanService service) {
        super(service);
    }
}
