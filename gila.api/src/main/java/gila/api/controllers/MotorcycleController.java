package gila.api.controllers;

import gila.core.entities.Motorcycle;
import gila.core.services.MotorcycleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController()
@RequestMapping("api/motorcycle")
public class MotorcycleController extends VehicleController<Motorcycle> {

    @Autowired
    public MotorcycleController(MotorcycleService service) {
        super(service);
    }
}
